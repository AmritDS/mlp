## Description
**Report1** and **Report2** contain project reports exploring the application of Multi-Task learning to Deep Convolutional Neural Networks, for classification of images in the CIFAR-100 data set. 

This work was completed as part of the Machine Learning Practical course at The University of Edinburgh, along with Mihaela Stoian and Tudor Feraiu.

## Outline
Convolutional neural networks are prone to overfitting to training data, and while conventional regularization approaches (such as L2 regularization or dropout) may be employed to curtail
over-fitting, they do not consider task domains while constraining weights. Multi-task learning with shared network-parameters is one way of constraining the learnt weights of a network in a task-aware manner, providing an alternative to conventional regularizing techniques. Specifically, it is possible to define auxillary-tasks related to a main task and constrain network weights within the general neighborhood of optimal values. In this work, we define multiple such auxiliary tasks using hierarchical clustering of varying levels of generality compared to a main image-classification task. We explore different clustering techniques for this purpose along with different bifurcation locations from a shared network. The reports detail the results of
our exploration with the VGG network architecure for multi-class image classification over the Cifar-100 image dataset.

![Combined Images](./combined_images.png)